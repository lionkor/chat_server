defmodule ChatServer do
  require Logger

  def accept(port) do
    {:ok, socket} = :gen_tcp.listen(port, [:binary, active: false, reuseaddr: true])
    Logger.info("Listening on port #{port}")
    loop_acceptor(socket)
  end

  def loop_acceptor(socket) do
    {:ok, client} = :gen_tcp.accept(socket)
    {:ok, pid} = Task.Supervisor.start_child(ChatServer.TaskSupervisor, fn -> serve(client) end)
    ChatServer.Broadcaster.add_client(ChatServer.Broadcaster, pid)
    :ok = :gen_tcp.controlling_process(client, pid)
    loop_acceptor(socket)
  end

  defp serve(socket) do
    Logger.info("Serving #{inspect(socket)}")
    :inet.setopts(socket, active: :once)
    receive do
      {:tcp, _from, data} ->
        ChatServer.Broadcaster.broadcast(ChatServer.Broadcaster, data)
      {:send, msg} ->
        :gen_tcp.send(socket, msg)
      {:tcp_error, _from, reason} ->
        Logger.error("TCP error: #{reason}")
        exit(:normal)
      {:tcp_closed, _from} ->
        Logger.info("Connection closed to #{inspect(socket)}")
        exit(:normal)
    end
    serve(socket)
  end

end
