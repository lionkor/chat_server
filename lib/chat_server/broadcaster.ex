defmodule ChatServer.Broadcaster do
  use GenServer
  require Logger

  @impl true
  def init(:ok) do
    clients = :ets.new(:clients, [:named_table, read_concurrency: true, write_concurrency: true])
    {:ok, clients}
  end

  @impl true
  def handle_call({:add_client, client}, _from, clients) do
    :ets.insert(clients, {client})
    Logger.info("Adding client #{inspect(client)}")
    {:reply, :ok, clients}
  end

  @impl true
  def handle_call({:remove_client, client}, _from, clients) do
    :ets.delete(clients, {client})
    Logger.info("Removing client #{inspect(client)}")
    {:reply, :ok, clients}
  end

  @impl true
  def handle_call({:broadcast, msg}, _from, clients) do
    Logger.info("Broadcasting #{inspect(msg)}")
    :ok = send_to(:ets.first(clients), clients, msg)
    {:reply, :ok, clients}
  end

  @impl true
  def handle_info({:DOWN, _ref, :process, pid, reason}, clients) do
    :ets.delete(clients, {pid})
    Logger.info("Client died: #{reason}")
    {:noreply, clients}
  end

  defp send_to(:'$end_of_table', _clients, _msg) do
    Logger.info("Done sending")
    :ok
  end

  defp send_to(client, clients, msg) do
    Logger.info("Sending #{inspect(msg)} to #{inspect(client)}")
    send(client, {:send, msg})
    send_to(:ets.next(clients, client), clients, msg)
  end

  def start_link(opts) do
    GenServer.start_link(__MODULE__, :ok, opts)
  end

  def add_client(server, client) do
    GenServer.call(server, {:add_client, client})
  end

  def remove_client(server, client) do
    GenServer.call(server, {:remove_client, client})
  end

  def broadcast(server, msg) do
    GenServer.call(server, {:broadcast, msg})
  end
end
