defmodule ChatServer.Application do
  # See https://hexdocs.pm/elixir/Application.html
  # for more information on OTP Applications
  @moduledoc false

  use Application

  @impl true
  def start(_type, _args) do
    children = [
      # Starts a worker by calling: ChatServer.Worker.start_link(arg)
      # {ChatServer.Worker, arg}
      {ChatServer.Broadcaster, name: ChatServer.Broadcaster},
      {Task.Supervisor, name: ChatServer.TaskSupervisor},
      Supervisor.child_spec({Task, fn -> ChatServer.accept(6969) end}, restart: :permanent)
    ]

    # See https://hexdocs.pm/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ChatServer.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
